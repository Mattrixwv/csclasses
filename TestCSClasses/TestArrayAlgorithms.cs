//C#/CSClasses/TestArrayAlgorithms.cs
//Matthew Ellison
// Created: 07-04-21
//Modified: 07-04-21
//This class contains tests for my array algorithms library
/*
Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Numerics;


namespace TestCSClasses{
	[TestClass]
	public class TestArrayAlgorithms{
		[TestMethod]
		public void TestGetSum(){
			//Test 1
			int correctAnswer = 0;
			List<int> numbers = new List<int>();
			int answer = mee.ArrayAlgorithms.GetSum(numbers);
			Assert.AreEqual(correctAnswer, answer, "GetSum Integer 1 failed");
			//Test 2
			correctAnswer = 118;
			numbers = new List<int>(){2, 2, 3, 3, 4, 4, 100};
			answer = mee.ArrayAlgorithms.GetSum(numbers);
			Assert.AreEqual(correctAnswer, answer, "GetSum Integer 2 failed");

			//Test 3
			long longCorrectAnswer = 118;
			List<long> longNumbers = new List<long>(){2, 2, 3, 3, 4, 4, 100};
			long longAnswer = mee.ArrayAlgorithms.GetSum(longNumbers);
			Assert.AreEqual(longCorrectAnswer, longAnswer, "GetSum Long failed");

			//Test 4
			BigInteger bigCorrectAnswer = 118;
			List<BigInteger> bigNumbers = new List<BigInteger>(){2, 2, 3, 3, 4, 4, 100};
			BigInteger bigAnswer = mee.ArrayAlgorithms.GetSum(bigNumbers);
			Assert.AreEqual(bigCorrectAnswer, bigAnswer, "GetSum BigInteger failed");
		}
		[TestMethod]
		public void TestGetProd(){
			//Test 1
			int correctAnswer = 0;
			List<int> numbers = new List<int>();
			int answer = mee.ArrayAlgorithms.GetProd(numbers);
			Assert.AreEqual(correctAnswer, answer, "GetProd Integer 1 failed");
			//Test 2
			correctAnswer = 57600;
			numbers = new List<int>(){2, 2, 3, 3, 4, 4, 100};
			answer = mee.ArrayAlgorithms.GetProd(numbers);
			Assert.AreEqual(correctAnswer, answer, "GetProd Integer 2 failed");

			//Test 3
			long longCorrectAnswer = 57600;
			List<long> longNumbers = new List<long>(){2, 2, 3, 3, 4, 4, 100};
			long longAnswer = mee.ArrayAlgorithms.GetProd(longNumbers);
			Assert.AreEqual(longCorrectAnswer, longAnswer, "GetProd Long failed");

			//Test 4
			BigInteger bigCorrectAnswer = 57600;
			List<BigInteger> bigNumbers = new List<BigInteger>(){2, 2, 3, 3, 4, 4, 100};
			BigInteger bigAnswer = mee.ArrayAlgorithms.GetProd(bigNumbers);
			Assert.AreEqual(bigCorrectAnswer, bigAnswer, "GetProd BigInteger failed");
		}
	}
}