//C#/CSClasses/TestNumberAlgorithms.cs
//Matthew Ellison
// Created: 07-04-21
//Modified: 07-04-21
//This class contains tests for my number algorithms library
/*
Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Numerics;


namespace TestNumberAlgorithms{
	[TestClass]
	public class TestNumberAlgorithms{
		[TestMethod]
		public void TestGetAllFib(){
			//Test 1
			List<int> correctAnswer = new List<int>(){1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89};
			int highestNumber = 100;
			List<int> answer = mee.NumberAlgorithms.GetAllFib(highestNumber);
			CollectionAssert.AreEqual(correctAnswer, answer, "GetAllFib Integer 1 failed");
			//Test 2
			correctAnswer = new List<int>(){1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987};
			highestNumber = 1000;
			answer = mee.NumberAlgorithms.GetAllFib(highestNumber);
			CollectionAssert.AreEqual(correctAnswer, answer, "GetAllFib Integer 2 failed");

			//Test 3
			List<long> longCorrectAnswer = new List<long>(){1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987};
			long longHighestNumber = 1000;
			List<long> longAnswer = mee.NumberAlgorithms.GetAllFib(longHighestNumber);
			CollectionAssert.AreEqual(longCorrectAnswer, longAnswer, "GetAllFib Long failed");

			//Test 4
			List<BigInteger> bigCorrectAnswer = new List<BigInteger>(){1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987};
			BigInteger bigHighestNumber = 1000;
			List<BigInteger> bigAnswer = mee.NumberAlgorithms.GetAllFib(bigHighestNumber);
			CollectionAssert.AreEqual(bigCorrectAnswer, bigAnswer, "GetAllFib BigInteger failed");
		}
		[TestMethod]
		public void TestGetFactors(){
			//Test 1
			List<int> correctAnswer = new List<int>(){2, 2, 5, 5};
			int number = 100;
			List<int> answer = mee.NumberAlgorithms.GetFactors(number);
			CollectionAssert.AreEqual(correctAnswer, answer, "GetFactors Integer 1 failed");
			//Test 2
			correctAnswer = new List<int>(){2, 7, 7};
			number = 98;
			answer = mee.NumberAlgorithms.GetFactors(number);
			CollectionAssert.AreEqual(correctAnswer, answer, "getFactors Integer 2 failed");

			//Test 3
			List<long> longCorrectAnswer = new List<long>(){2, 2, 5, 5};
			long longNumber = 100;
			List<long> longAnswer = mee.NumberAlgorithms.GetFactors(longNumber);
			CollectionAssert.AreEqual(longCorrectAnswer, longAnswer, "GetFactors Long failed");

			//Test 4
			List<BigInteger> bigCorrectAnswer = new List<BigInteger>(){2, 2, 5, 5};
			BigInteger bigNumber = 100;
			List<BigInteger> bigAnswer = mee.NumberAlgorithms.GetFactors(bigNumber);
			CollectionAssert.AreEqual(bigCorrectAnswer, bigAnswer, "GetFactors BigInteger failed");
		}
		[TestMethod]
		public void TestGetPrimes(){
			//Test 1
			List<int> correctAnswer = new List<int>(){2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
			int topNum = 100;
			List<int> answer = mee.NumberAlgorithms.GetPrimes(topNum);
			CollectionAssert.AreEqual(correctAnswer, answer, "GetPrimes Integer failed");

			//Test 2
			List<long> longCorrectAnswer = new List<long>(){2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
			long longTopNum = 100;
			List<long> longAnswer = mee.NumberAlgorithms.GetPrimes(longTopNum);
			CollectionAssert.AreEqual(longCorrectAnswer, longAnswer, "GetPrimes Long failed");

			//Test 3
			List<BigInteger> bigCorrectAnswer = new List<BigInteger>(){2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
			BigInteger bigTopNum = 100;
			List<BigInteger> bigAnswer = mee.NumberAlgorithms.GetPrimes(bigTopNum);
			CollectionAssert.AreEqual(bigCorrectAnswer, bigAnswer, "GetPrimes BigInteger failed");
		}
		[TestMethod]
		public void TestGetNumPrimes(){
			//Test 1
			List<int> correctAnswer = new List<int>(){2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
			int numPrimes = 25;
			List<int> answer = mee.NumberAlgorithms.GetNumPrimes(numPrimes);
			CollectionAssert.AreEqual(correctAnswer, answer, "GetNumPrimes Integer failed");

			//Test 2
			List<long> longCorrectAnswer = new List<long>(){2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
			long longNumPrimes = 25;
			List<long> longAnswer = mee.NumberAlgorithms.GetNumPrimes(longNumPrimes);
			CollectionAssert.AreEqual(longCorrectAnswer, longAnswer, "GetNumPrimes Long failed");

			//Test 3
			List<BigInteger> bigCorrectAnswer = new List<BigInteger>(){2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
			BigInteger bigNumPrimes = 25;
			List<BigInteger> bigAnswer = mee.NumberAlgorithms.GetNumPrimes(bigNumPrimes);
			CollectionAssert.AreEqual(bigCorrectAnswer, bigAnswer, "GetNumPrimes BigInteger failes");
		}
		[TestMethod]
		public void TestIsPrime(){
			//Test 1
			int num = 2;
			bool correctAnswer = true;
			bool answer = mee.NumberAlgorithms.IsPrime(num);
			Assert.AreEqual(correctAnswer, answer, "IsPrime 1 failed");
			//Test 2
			num = 97;
			correctAnswer = true;
			answer = mee.NumberAlgorithms.IsPrime(num);
			Assert.AreEqual(correctAnswer, answer, "IsPrime 2 failed");
			//Test 3
			num = 1000;
			correctAnswer = false;
			answer = mee.NumberAlgorithms.IsPrime(num);
			Assert.AreEqual(correctAnswer, answer, "IsPrime 3 failed");
			//Test 4
			num = 1;
			correctAnswer = false;
			answer = mee.NumberAlgorithms.IsPrime(num);
			Assert.AreEqual(correctAnswer, answer, "IsPrime 4 failed");

			//Test 5
			long longNum = 2;
			correctAnswer = true;
			answer = mee.NumberAlgorithms.IsPrime(longNum);
			Assert.AreEqual(correctAnswer, answer, "IsPrime long 1 failed");
			//Test 6
			longNum = 97;
			correctAnswer = true;
			answer = mee.NumberAlgorithms.IsPrime(longNum);
			Assert.AreEqual(correctAnswer, answer, "IsPrime long 2 failed");
			//Test 7
			longNum = 1000;
			correctAnswer = false;
			answer = mee.NumberAlgorithms.IsPrime(longNum);
			Assert.AreEqual(correctAnswer, answer, "IsPrime long 3 failed");
			//Test 8
			longNum = 1;
			correctAnswer = false;
			answer = mee.NumberAlgorithms.IsPrime(longNum);
			Assert.AreEqual(correctAnswer, answer, "IsPrime long 4 failed");

			//Test 9
			BigInteger bigNum = 2;
			correctAnswer = true;
			answer = mee.NumberAlgorithms.IsPrime(bigNum);
			Assert.AreEqual(correctAnswer, answer, "IsPrime Big 1 failed");
			//Test 10
			bigNum = 97;
			correctAnswer = true;
			answer = mee.NumberAlgorithms.IsPrime(bigNum);
			Assert.AreEqual(correctAnswer, answer, "IsPrime Big 2 failed");
			//Test 11
			bigNum = 1000;
			correctAnswer = false;
			answer = mee.NumberAlgorithms.IsPrime(bigNum);
			Assert.AreEqual(correctAnswer, answer, "IsPrime Big 3 failed");
			//Test 12
			bigNum = 1;
			correctAnswer = false;
			answer = mee.NumberAlgorithms.IsPrime(bigNum);
			Assert.AreEqual(correctAnswer, answer, "IsPrime Big 4 failed");
		}
		[TestMethod]
		public void TestGetDivisors(){
			//Test 1
			List<int> correctAnswer = new List<int>(){1, 2, 4, 5, 10, 20, 25, 50, 100};
			int topNum = 100;
			List<int> answer = mee.NumberAlgorithms.GetDivisors(topNum);
			CollectionAssert.AreEqual(correctAnswer, answer, "GetDivisors Integer failed");

			//Test 2
			List<long> longCorrectAnswer = new List<long>(){1, 2, 4, 5, 10, 20, 25, 50, 100};
			long longTopNum = 100;
			List<long> longAnswer = mee.NumberAlgorithms.GetDivisors(longTopNum);
			CollectionAssert.AreEqual(longCorrectAnswer, longAnswer, "GetDivisors Long failed");

			//Test 3
			List<BigInteger> bigCorrectAnswer = new List<BigInteger>(){1, 2, 4, 5, 10, 20, 25, 50, 100};
			BigInteger bigTopNum = 100;
			List<BigInteger> bigAnswer = mee.NumberAlgorithms.GetDivisors(bigTopNum);
			CollectionAssert.AreEqual(bigCorrectAnswer, bigAnswer, "GetDivisors BigInteger failed");
		}
		[TestMethod]
		public void TestGetFib(){
			//Test 1
			int correctAnswer = 144;
			int number = 12;
			int answer = mee.NumberAlgorithms.GetFib(number);
			Assert.AreEqual(correctAnswer, answer, "GetFib Integer 1 failed");
			//Test 2
			correctAnswer = 6765;
			number = 20;
			answer = mee.NumberAlgorithms.GetFib(number);
			Assert.AreEqual(correctAnswer, answer, "GetFib Integer 2 failed");

			//Test 3
			long longCorrectAnswer = 6765;
			long longNumber = 20;
			long longAnswer = mee.NumberAlgorithms.GetFib(longNumber);
			Assert.AreEqual(longCorrectAnswer, longAnswer, "GetFib Long failed");

			//Test 4
			BigInteger bigCorrectAnswer = BigInteger.Parse("1070066266382758936764980584457396885083683896632151665013235203375314520604694040621889147582489792657804694888177591957484336466672569959512996030461262748092482186144069433051234774442750273781753087579391666192149259186759553966422837148943113074699503439547001985432609723067290192870526447243726117715821825548491120525013201478612965931381792235559657452039506137551467837543229119602129934048260706175397706847068202895486902666185435124521900369480641357447470911707619766945691070098024393439617474103736912503231365532164773697023167755051595173518460579954919410967778373229665796581646513903488154256310184224190259846088000110186255550245493937113651657039447629584714548523425950428582425306083544435428212611008992863795048006894330309773217834864543113205765659868456288616808718693835297350643986297640660000723562917905207051164077614812491885830945940566688339109350944456576357666151619317753792891661581327159616877487983821820492520348473874384736771934512787029218636250627816");
			BigInteger bigNumber = 4782;
			BigInteger bigAnswer = mee.NumberAlgorithms.GetFib(bigNumber);
			Assert.AreEqual(bigCorrectAnswer, bigAnswer, "getFib BigInteger failed");
		}
		[TestMethod]
		public void TestGCD(){
			//Test 1
			int num1 = 2;
			int num2 = 3;
			int correctAnswer = 1;
			int answer = mee.NumberAlgorithms.GCD(num1, num2);
			Assert.AreEqual(correctAnswer, answer, "GCD Integer 1 failed");
			//Test 2
			num1 = 1000;
			num2 = 575;
			correctAnswer = 25;
			answer = mee.NumberAlgorithms.GCD(num1, num2);
			Assert.AreEqual(correctAnswer, answer, "GCD Integer 2 failed");
			//Test 3
			num1 = 1000;
			num2 = 1000;
			correctAnswer = 1000;
			answer = mee.NumberAlgorithms.GCD(num1, num2);
			Assert.AreEqual(correctAnswer, answer, "GCD Integer 3 failed");

			//Test 4
			long longNum1 = 2;
			long longNum2 = 3;
			long longCorrectAnswer = 1;
			long longAnswer = mee.NumberAlgorithms.GCD(longNum1, longNum2);
			Assert.AreEqual(longCorrectAnswer, longAnswer, "GCD Long 1 failed");
			//Test 5
			longNum1 = 1000;
			longNum2 = 575;
			longCorrectAnswer = 25;
			longAnswer = mee.NumberAlgorithms.GCD(longNum1, longNum2);
			Assert.AreEqual(longCorrectAnswer, longAnswer, "GCD Long 2 failed");
			//Test 6
			longNum1 = 1000;
			longNum2 = 1000;
			longCorrectAnswer = 1000;
			longAnswer = mee.NumberAlgorithms.GCD(longNum1, longNum2);
			Assert.AreEqual(longCorrectAnswer, longAnswer, "GCD Long 3 failed");

			//Test 7
			BigInteger bigNum1 = 2;
			BigInteger bigNum2 = 3;
			BigInteger bigCorrectAnswer = 1;
			BigInteger bigAnswer = mee.NumberAlgorithms.GCD(bigNum1, bigNum2);
			Assert.AreEqual(bigCorrectAnswer, bigAnswer, "GCD BigInteger 1 failed");
			//Test 8
			bigNum1 = 1000;
			bigNum2 = 575;
			bigCorrectAnswer = 25;
			bigAnswer = mee.NumberAlgorithms.GCD(bigNum1, bigNum2);
			Assert.AreEqual(bigCorrectAnswer, bigAnswer, "GCD BigInteger 2 failed");
			//Test 9
			bigNum1 = 1000;
			bigNum2 = 1000;
			bigCorrectAnswer = 1000;
			bigAnswer = mee.NumberAlgorithms.GCD(bigNum1, bigNum2);
			Assert.AreEqual(bigCorrectAnswer, bigAnswer, "GCD BigInteger 3 failed");
		}
		[TestMethod]
		public void TestFactorial(){
			//Test 1
			int num = 1;
			int correctAnswer = 1;
			int answer = mee.NumberAlgorithms.Factorial(num);
			Assert.AreEqual(correctAnswer, answer, "Factorial 1 failed");
			//Test 2
			num = 10;
			correctAnswer = 3628800;
			answer = mee.NumberAlgorithms.Factorial(num);
			Assert.AreEqual(correctAnswer, answer, "Factorial 2 failed");
			//Test 3
			num = -5;
			correctAnswer = 1;
			answer = mee.NumberAlgorithms.Factorial(num);
			Assert.AreEqual(correctAnswer, answer, "Factorial 3 failed");

			//Test 4
			long numLong = 10;
			long correctAnswerLong = 3628800;
			long answerLong = mee.NumberAlgorithms.Factorial(numLong);
			Assert.AreEqual(correctAnswerLong, answerLong, "Factorial 4 failed");

			//Test 5
			BigInteger numBig = 10;
			BigInteger correctAnswerBig = 3628800;
			BigInteger answerBig = mee.NumberAlgorithms.Factorial(numBig);
			Assert.AreEqual(correctAnswerBig, answerBig, "Factorial 5 failed");
		}
		[TestMethod]
		public void TestToBin(){
			//Test 1
			int num = 7;
			string correctAnswer = "111";
			string answer = mee.NumberAlgorithms.ToBin(num);
			Assert.AreEqual(correctAnswer, answer, "ToBin 1 failed");
			//Test 2
			num = 0;
			correctAnswer = "0";
			answer = mee.NumberAlgorithms.ToBin(num);
			Assert.AreEqual(correctAnswer, answer, "ToBin 2 failed");
			//Test 3
			num = 1000000;
			correctAnswer = "11110100001001000000";
			answer = mee.NumberAlgorithms.ToBin(num);
			Assert.AreEqual(correctAnswer, answer, "ToBin 3 failed");

			//Test 4
			long longNum = 7;
			correctAnswer = "111";
			answer = mee.NumberAlgorithms.ToBin(longNum);
			Assert.AreEqual(correctAnswer, answer, "ToBing long 1 failed");
			//Test 5
			longNum = 0;
			correctAnswer = "0";
			answer = mee.NumberAlgorithms.ToBin(longNum);
			Assert.AreEqual(correctAnswer, answer, "ToBin long 2 failed");
			//Test 6
			longNum = 1000000;
			correctAnswer = "11110100001001000000";
			answer = mee.NumberAlgorithms.ToBin(longNum);
			Assert.AreEqual(correctAnswer, answer, "ToBing long 3 failed");

			//Test 7
			BigInteger bigNum = 7;
			correctAnswer = "111";
			answer = mee.NumberAlgorithms.ToBin(bigNum);
			Assert.AreEqual(correctAnswer, answer, "ToBin big 1 failed");
			//Test 8
			bigNum = 0;
			correctAnswer = "0";
			answer = mee.NumberAlgorithms.ToBin(bigNum);
			Assert.AreEqual(correctAnswer, answer, "ToBin big 2 failed");
			//Test 9
			bigNum = 1000000;
			correctAnswer = "11110100001001000000";
			answer = mee.NumberAlgorithms.ToBin(bigNum);
			Assert.AreEqual(correctAnswer, answer, "ToBin big 3 failed");
		}
		[TestMethod]
		public void TestSieveOfEratosthenes(){
			//Test 1
			IEnumerator<long> sieve = mee.NumberAlgorithms.SieveOfEratosthenes().GetEnumerator();
			List<long> correctAnswer = new List<long>(){2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
			List<long> answer = new List<long>();
			for(int cnt = 0;cnt < 25;++cnt){
				sieve.MoveNext();
				answer.Add(sieve.Current);
			}
			CollectionAssert.AreEqual(correctAnswer, answer, "SieveOfEratosthenes failed");

			//Test 2
			IEnumerator<BigInteger> bigSieve = mee.NumberAlgorithms.SieveOfEratosthenesBig().GetEnumerator();
			List<BigInteger> bigCorrectAnswer = new List<BigInteger>(){2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
			List<BigInteger> bigAnswer = new List<BigInteger>();
			for(int cnt = 0;cnt < 25;++cnt){
				bigSieve.MoveNext();
				bigAnswer.Add(bigSieve.Current);
			}
			CollectionAssert.AreEqual(bigCorrectAnswer, bigAnswer, "SieveOfEratosthenesBig failed");
		}
	}
}