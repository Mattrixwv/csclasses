//C#/CSClasses/TestStringAlgorithms.cs
//Matthew Ellison
// Created: 07-04-21
//Modified: 07-04-21
//This class contains tests for my string algorithms library
/*
Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;


namespace TestStringAlgorithms{
	[TestClass]
	public class TestStringAlgorithms{
		[TestMethod]
		public void TestGetPermutations(){
			//Test 1
			string permString = "012";
			List<string> correctAnswer = new List<string>(){"012", "021", "102", "120", "201", "210"};
			List<string> answer = mee.StringAlgorithms.GetPermutations(permString);
			CollectionAssert.AreEqual(correctAnswer, answer, "GetPermutations failed");
		}
		[TestMethod]
		public void TestFindNumOccurrence(){
			//Test 1
			string testString = "abcdefgdd";
			char testChar = 'a';
			long correctAnswer = 1;
			long answer = mee.StringAlgorithms.FindNumOccurrence(testString, testChar);
			Assert.AreEqual(correctAnswer, answer, "FindNumOccurrence 1 failed");
			//Test 2
			testChar = 'd';
			correctAnswer = 3;
			answer = mee.StringAlgorithms.FindNumOccurrence(testString, testChar);
			Assert.AreEqual(correctAnswer, answer, "FindNumOccurrence 2 failed");
			//Test 3
			testChar = 'h';
			correctAnswer = 0;
			answer = mee.StringAlgorithms.FindNumOccurrence(testString, testChar);
			Assert.AreEqual(correctAnswer, answer, "FindNumOccurrence 3 failed");
		}
		[TestMethod]
		public void TestIsPalindrome(){
			//Test 1
			string str = "101";
			bool correctAnswer = true;
			bool answer = mee.StringAlgorithms.IsPalindrome(str);
			Assert.AreEqual(correctAnswer, answer, "IsPalindrome 1 failed");

			//Test 2
			str = "100";
			correctAnswer = false;
			answer = mee.StringAlgorithms.IsPalindrome(str);
			Assert.AreEqual(correctAnswer, answer, "IsPalindrome 2 failed");

			//Test 3
			str = "";
			correctAnswer = true;
			answer = mee.StringAlgorithms.IsPalindrome(str);
			Assert.AreEqual(correctAnswer, answer, "IsPalindrome 3 failed");
		}
		[TestMethod]
		public void TestPrintList(){
			//Test 1
			List<int> nums = new List<int>();
			string correctAnswer = "[]";
			string answer = mee.StringAlgorithms.PrintList(nums);
			Assert.AreEqual(correctAnswer, answer, "PrintList<int> 1 failed");
			//Test 2
			nums = new List<int>(){1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
			correctAnswer = "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]";
			answer = mee.StringAlgorithms.PrintList(nums);
			Assert.AreEqual(correctAnswer, answer, "PrintList<int> 2 failed");
			//Test 3
			nums = new List<int>(){-3, -2, -1, 0, 1, 2, 3};
			correctAnswer = "[-3, -2, -1, 0, 1, 2, 3]";
			answer = mee.StringAlgorithms.PrintList(nums);
			Assert.AreEqual(correctAnswer, answer, "PrintList<int> 3 failed");

			//Test 4
			List<string> strings = new List<string>(){"A", "B", "C"};
			correctAnswer = "[A, B, C]";
			answer = mee.StringAlgorithms.PrintList(strings);
			Assert.AreEqual(correctAnswer, answer, "PrintList<string> 1 failed");
			//Test 5
			strings = new List<string>(){"abc", "def", "ghi"};
			correctAnswer = "[abc, def, ghi]";
			answer = mee.StringAlgorithms.PrintList(strings);
			Assert.AreEqual(correctAnswer, answer, "PrintList<string> 2 failed");
		}
		[TestMethod]
		public void TestPrintArray(){
			//Test 1
			int[] nums = new int[]{};
			string correctAnswer = "[]";
			string answer = mee.StringAlgorithms.PrintArray(nums);
			Assert.AreEqual(correctAnswer, answer, "PrintArray<int> 1 failed");
			//Test 2
			nums = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
			correctAnswer = "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]";
			answer = mee.StringAlgorithms.PrintArray(nums);
			Assert.AreEqual(correctAnswer, answer, "PrintArray<int> 2 failed");
			//Test 3
			nums = new int[]{-3, -2, -1, 0, 1, 2, 3};
			correctAnswer = "[-3, -2, -1, 0, 1, 2, 3]";
			answer = mee.StringAlgorithms.PrintArray(nums);
			Assert.AreEqual(correctAnswer, answer, "PrintArray<int> 3 failed");

			//Test 4
			string[] strings = new string[]{"A", "B", "C"};
			correctAnswer = "[A, B, C]";
			answer = mee.StringAlgorithms.PrintArray(strings);
			Assert.AreEqual(correctAnswer, answer, "PrintArray<string> 1 failed");
			//Test 5
			strings = new string[]{"abc", "def", "ghi"};
			correctAnswer = "[abc, def, ghi]";
			answer = mee.StringAlgorithms.PrintArray(strings);
			Assert.AreEqual(correctAnswer, answer, "PrintArray<string> 2 failed");
		}
	}
}