namespace mee.Exceptions{
	[System.Serializable]
	public class InvalidResult : System.Exception{
		public InvalidResult(){
		}
		public InvalidResult(string message) : base(message){
		}
		public InvalidResult(string message, System.Exception inner) : base(message, inner){
		}
		protected InvalidResult(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context){
		}
	}
}
