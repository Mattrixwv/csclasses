//C#/CSClasses/ArrayAlgorithms.cs
//Matthew Ellison
// Created: 07-04-21
//Modified: 07-04-21
//This class contains functions that perform algorithms on numbers
/*
Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;
using System.Linq;
using System.Numerics;


namespace mee{
	public class ArrayAlgorithms{
		//These functions get a value from combining elements in an array
		public static int GetSum(List<int> ary){
			return ary.Sum();
		}
		public static long GetSum(List<long> ary){
			return ary.Sum();
		}
		public static BigInteger GetSum(List<BigInteger> ary){
			BigInteger sum = 0;
			foreach(BigInteger num in ary){
				sum += num;
			}
			return sum;
		}
		public static int GetProd(List<int> ary){
			//If a blank list was passed to the function return 0 as the product
			if(ary.Capacity == 0){
				return 0;
			}

			int prod = 1;	//Start at 1 because x * 1 = x
			foreach(int num in ary){
				prod *= num;
			}
			return prod;
		}
		public static long GetProd(List<long> ary){
			//If a blank list was passed to the function return 0 as the product
			if(ary.Capacity == 0){
				return 0;
			}

			long prod = 1;	//Start at 1 because x * 1 = x
			foreach(long num in ary){
				prod *= num;
			}
			return prod;
		}
		public static BigInteger GetProd(List<BigInteger> ary){
			//If a blank list was passed to the function return 0 as the product
			if(ary.Capacity == 0){
				return 0;
			}

			BigInteger prod = 1;	//Start at 1 because x * 1 = x
			foreach(BigInteger num in ary){
				prod *= num;
			}
			return prod;
		}
	}
}