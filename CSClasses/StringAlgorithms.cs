//C#/CSClasses/StringAlgorithms.cs
//Matthew Ellison
// Created: 07-04-21
//Modified: 07-04-21
//This class contains functions that perform algorithms on numbers
/*
Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;
using System.Linq;



namespace mee{
	public class StringAlgorithms{
		//This is a function the creates all permutations of a string and returns a list of those permutations
		public static List<string> GetPermutations(string master){
			return GetPermutations(master, 0);
		}
		public static List<string> GetPermutations(string master, int num){
			List<string> perms = new List<string>();
			//Check if the number is out of bounds
			if((num >= master.Length) || (num < 0)){
				//Do nothing and return an empty list
			}
			//If this is the last possible recurse just return the current string
			else if(num == (master.Length - 1)){
				perms.Add(master);
			}
			//If there are more possible recurses, the recurse with the current permutation
			else{
				List<string> temp = GetPermutations(master, num + 1);
				perms.AddRange(temp);
				//You need to swap the current letter with every possible letter after it
				//The ones needed to swap before will happen automatically when the function recurses
				for(int cnt = 1;(num + cnt) < master.Length;++cnt){
					master = SwapString(master, num, (num + cnt));
					temp = GetPermutations(master, num + 1);
					perms.AddRange(temp);
					master = SwapString(master, num, (num + cnt));
				}

				//The array is not necessarily in alpha-numeric order. So if this is the full array sort it before returning
				if(num == 0){
					perms.Sort();
				}
			}

			//Return the list that was build
			return perms;
		}
		public static string SwapString(string str, int first, int second){
			char[] tempStr = str.ToCharArray();
			char temp = str[first];
			tempStr[first] = tempStr[second];
			tempStr[second] = temp;
			return new string(tempStr);
		}
		//This function returns the number of times the character occurs in the string
		public static long FindNumOccurrence(string str, char c){
			return str.Count(ch => ch == c);
		}
		//Returns true if the string passed in is a palindrome
		public static bool IsPalindrome(string str){
			string rev = new string(str.ToCharArray().Reverse().ToArray());
			if(str == rev){
				return true;
			}
			else{
				return false;
			}
		}
		//Print a list
		public static string PrintList<T>(List<T> list){
			string listString = "[";
			for(int cnt = 0;cnt < list.Count;++cnt){
				listString += list[cnt];
				if(cnt < list.Count - 1){
					listString += ", ";
				}
			}
			listString += "]";
			return listString;
		}
		//Print an array
		public static string PrintArray<T>(T[] array){
			string arrayString = "[";
			for(int cnt = 0;cnt < array.Length;++cnt){
				arrayString += array[cnt];
				if(cnt < array.Length - 1){
					arrayString += ", ";
				}
			}
			arrayString += "]";
			return arrayString;
		}
	}
}