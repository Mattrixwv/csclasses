//C#/CSClasses/NumberAlgorithms.cs
//Matthew Ellison
// Created: 07-04-21
//Modified: 07-04-21
//This class contains functions that perform algorithms on numbers
/*
Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;


namespace mee{
	public class NumberAlgorithms{
		//Returns the sqrt of the BigInt passed to it
		public static BigInteger BigIntegerSqrt(BigInteger num){
			BigInteger sqrt;

			//Get the sqrt from the general formula
			double tempSqrt = Math.Exp(BigInteger.Log(num) / 2);
			//Account for floating-point rounding errors
			if((BigInteger)(tempSqrt * tempSqrt) < num){
				sqrt = (BigInteger)Math.Ceiling(tempSqrt);
			}
			else{
				sqrt = (BigInteger)tempSqrt;
			}
			return sqrt;
		}
		//These functions return a list of all Fibonacci numbers <= goalNumber
		public static List<int> GetAllFib(int goalNumber){
			//Setup the variables
			List<int> fibNums = new List<int>();

			//If the number is <= 0 return an empty list
			if(goalNumber <= 0){
				return fibNums;
			}

			//This means that at least 2 1's are elements
			fibNums.Add(1);
			fibNums.Add(1);

			//Loop to generate the rest of the Fibonacci numbers
			while(fibNums[fibNums.Count - 1] <= goalNumber){
				fibNums.Add(fibNums[fibNums.Count - 1] + fibNums[fibNums.Count - 2]);
			}

			//At this point the most recent number is > goalNumber, so remove it and return the rest of the list
			fibNums.RemoveAt(fibNums.Count - 1);
			return fibNums;
		}
		public static List<long> GetAllFib(long goalNumber){
			//Setup the variables
			List<long> fibNums = new List<long>();

			//If the number is <= 0 return an empty list
			if(goalNumber <= 0){
				return fibNums;
			}

			//This means that at least 2 1's are elements
			fibNums.Add(1);
			fibNums.Add(1);

			//Loop to generate the rest of the Fibonacci numbers
			while(fibNums[fibNums.Count - 1] <= goalNumber){
				fibNums.Add(fibNums[fibNums.Count - 1] + fibNums[fibNums.Count - 2]);
			}

			//At this point the most recent number is > goalNumber, so remove it and return the rest of the list
			fibNums.RemoveAt(fibNums.Count - 1);
			return fibNums;
		}
		public static List<BigInteger> GetAllFib(BigInteger goalNumber){
			//Setup the variables
			List<BigInteger> fibNums = new List<BigInteger>();

			//If the number is <= 0 return am empty list
			if(goalNumber <= 0){
				return fibNums;
			}

			//This means that at least 2 1's are elements
			fibNums.Add(1);
			fibNums.Add(1);

			//Loop to generate the rest of Fibonacci numbers
			while(fibNums[fibNums.Count - 1] <= goalNumber){
				fibNums.Add(fibNums[fibNums.Count - 1] + fibNums[fibNums.Count - 2]);
			}

			//At this point the most recent number is > goalNumber, so remove it and return the rest of the list
			fibNums.RemoveAt(fibNums.Count - 1);
			return fibNums;
		}
		//These functions return all factors of goalNumber
		public static List<int> GetFactors(int goalNumber){
			//You need to get all the primes that could be factors of this number so you can test them
			int topPossiblePrime = (int)Math.Ceiling(Math.Sqrt(goalNumber));
			List<int> primes = GetPrimes(topPossiblePrime);
			List<int> factors = new List<int>();

			//YOu need to step through each prime and see if it is a factor in the number
			for(int cnt = 0;cnt < primes.Count;){
				//If the prime is a factor you need to add it to the factor list
				if((goalNumber % primes[cnt]) == 0){
					factors.Add(primes[cnt]);
					goalNumber /= primes[cnt];
				}
				//Otherwise advance the location in primes you are looking at
				//By noit advancing if the prime is a factor you allow for multiple of the same prime as a factor
				else{
					++cnt;
				}
			}

			//If you didn't get any factors the number itself must be a prime
			if(factors.Count == 0){
				factors.Add(goalNumber);
				goalNumber /= goalNumber;
			}

			//If for some reason the goalNumber is not 1 throw an exception
			if(goalNumber != 1){
				throw new mee.Exceptions.InvalidResult("Factor did not end as 1");
			}

			//Return the list of factors
			return factors;
		}
		public static List<long> GetFactors(long goalNumber){
			//You need to get all the primes that could be factors of this number so you can test them
			long topPossiblePrime = (long)Math.Ceiling(Math.Sqrt(goalNumber));
			List<long> primes = GetPrimes(topPossiblePrime);
			List<long> factors = new List<long>();

			//YOu need to step through each prime and see if it is a factor in the number
			for(int cnt = 0;cnt < primes.Count;){
				//If the prime is a factor you need to add it to the factor list
				if((goalNumber % primes[cnt]) == 0){
					factors.Add(primes[cnt]);
					goalNumber /= primes[cnt];
				}
				//Otherwise advance the location in primes you are looking at
				//By noit advancing if the prime is a factor you allow for multiple of the same prime as a factor
				else{
					++cnt;
				}
			}

			//If you didn't get any factors the number itself must be a prime
			if(factors.Count == 0){
				factors.Add(goalNumber);
				goalNumber /= goalNumber;
			}

			//If for some reason the goalNumber is not 1 throw an exception
			if(goalNumber != 1){
				throw new mee.Exceptions.InvalidResult("Factor did not end as 1");
			}

			//Return the list of factors
			return factors;
		}
		public static List<BigInteger> GetFactors(BigInteger goalNumber){
			//You need to get all the primes that could be factors of this number so you can test them
			BigInteger topPossiblePrime = (BigInteger)Math.Exp(BigInteger.Log(goalNumber) / 2);
			List<BigInteger> primes = GetPrimes(topPossiblePrime);
			List<BigInteger> factors = new List<BigInteger>();

			//YOu need to step through each prime and see if it is a factor in the number
			for(int cnt = 0;cnt < primes.Count;){
				//If the prime is a factor you need to add it to the factor list
				if((goalNumber % primes[cnt]) == 0){
					factors.Add(primes[cnt]);
					goalNumber /= primes[cnt];
				}
				//Otherwise advance the location in primes you are looking at
				//By noit advancing if the prime is a factor you allow for multiple of the same prime as a factor
				else{
					++cnt;
				}
			}

			//If you didn't get any factors the number itself must be a prime
			if(factors.Count == 0){
				factors.Add(goalNumber);
				goalNumber /= goalNumber;
			}

			//If for some reason the goalNumber is not 1 throw an exception
			if(goalNumber != 1){
				throw new mee.Exceptions.InvalidResult("Factor did not end as 1");
			}

			//Return the list of factors
			return factors;
		}
		//These functions return a list with all the prime number <= goalNumber
		public static List<int> GetPrimes(int goalNumber){
			List<int> primes = new List<int>();	//Holds the prime numbers
			bool foundFactor = false;	//A flag for whether a factor of the current number has been found

			//If the number is 0 or negative return an empty list
			if(goalNumber <= 1){
				return primes;
			}
			//Otherwise the number is at least 2, so 2 should be added to the list
			else{
				primes.Add(2);
			}

			//We can now start at 3 and skip all even numbers, because they cannot be prime
			for(int possiblePrime = 3;possiblePrime <= goalNumber;possiblePrime += 2){
				//Check all current primes, up to sqrt(possiblePrime), to see if there is a divisor
				int topPossibleFactor = (int)Math.Ceiling(Math.Sqrt(possiblePrime));
				//We can safely assume that there will be at least 1 element in the primes list because of 2 being added before this
				for(int primesCnt = 0;primes[primesCnt] <= topPossibleFactor;){
					if((possiblePrime % primes[primesCnt]) == 0){
						foundFactor = true;
						break;
					}
					else{
						++primesCnt;
					}
					//Check if the index has gone out of range
					if(primesCnt >= primes.Count){
						break;
					}
				}

				//If you didn't find a factor then the current number must be prime
				if(!foundFactor){
					primes.Add(possiblePrime);
				}
				else{
					foundFactor = false;
				}
			}
			//Sort the list before returning it
			primes.Sort();
			return primes;
		}
		public static List<long> GetPrimes(long goalNumber){
			List<long> primes = new List<long>();	//Holds the prime numbers
			bool foundFactor = false;	//A flag for whether a factor of the current number has been found

			//If the number is 0 or negative return an empty list
			if(goalNumber <= 1){
				return primes;
			}
			//Otherwise the number is at least 2, so 2 should be added to the list
			else{
				primes.Add(2);
			}

			//We can now start at 3 and skip all even numbers, because they cannot be prime
			for(long possiblePrime = 3;possiblePrime <= goalNumber;possiblePrime += 2){
				//Check all current primes, up to sqrt(possiblePrime), to see if there is a divisor
				long topPossibleFactor = (long)Math.Ceiling(Math.Sqrt(possiblePrime));
				//We can safely assume that there will be at least 1 element in the primes list because of 2 being added before this
				for(int primesCnt = 0;primes[primesCnt] <= topPossibleFactor;){
					if((possiblePrime % primes[primesCnt]) == 0){
						foundFactor = true;
						break;
					}
					else{
						++primesCnt;
					}
					//Check if the index has gone out of range
					if(primesCnt >= primes.Count){
						break;
					}
				}

				//If you didn't find a factor then the current number must be prime
				if(!foundFactor){
					primes.Add(possiblePrime);
				}
				else{
					foundFactor = false;
				}
			}
			//Sort the list before returning it
			primes.Sort();
			return primes;
		}
		public static List<BigInteger> GetPrimes(BigInteger goalNumber){
			List<BigInteger> primes = new List<BigInteger>();	//Holds the prime numbers
			bool foundFactor = false;	//A flag for whether a factor of the current number has been found

			//If the number is 0 or negative return an empty list
			if(goalNumber <= 1){
				return primes;
			}
			//Otherwise the number is at least 2, so 2 should be added to the list
			else{
				primes.Add(2);
			}

			//We can now start at 3 and skip all even numbers, because they cannot be prime
			for(BigInteger possiblePrime = 3;possiblePrime <= goalNumber;possiblePrime += 2){
				//Check all current primes, up to sqrt(possiblePrime), to see if there is a divisor
				BigInteger topPossibleFactor = BigIntegerSqrt(possiblePrime);
				//We can safely assume that there will be at least 1 element in the primes list because of 2 being added before this
				for(int primesCnt = 0;primes[primesCnt] <= topPossibleFactor;){
					if((possiblePrime % primes[primesCnt]) == 0){
						foundFactor = true;
						break;
					}
					else{
						++primesCnt;
					}
					//Check if the index has gone out of range
					if(primesCnt >= primes.Count){
						break;
					}
				}

				//If you didn't find a factor then the current number must be prime
				if(!foundFactor){
					primes.Add(possiblePrime);
				}
				else{
					foundFactor = false;
				}
			}

			//Sort the list before returning it
			primes.Sort();
			return primes;
		}
		//This function gets a certain number of primes
		public static List<int> GetNumPrimes(int numberOfPrimes){
			List<int> primes = new List<int>();	//Holds the prime numbers
			bool foundFactor = false;	//A flag for whether a factor of the current number has been found

			//If the number is 0 or negative return an empty list
			if(numberOfPrimes <= 1){
				return primes;
			}
			//Otherwise the number is at least 2, so 2 should be added to the list
			else{
				primes.Add(2);
			}

			//We can now start at 3 and skip all even numbers, because they cannot be prime
			for(int possiblePrime = 3;primes.Count < numberOfPrimes;possiblePrime += 2){
				//Check all current primes, up to sqrt(possiblePrime), to see if there is a divisor
				int topPossibleFactor = (int)Math.Ceiling(Math.Sqrt(possiblePrime));
				//We can safely assume that there will be at least 1 element in the primes list because of 2 being added before this
				for(int primesCnt = 0;primes[primesCnt] <= topPossibleFactor;){
					if((possiblePrime % primes[primesCnt]) == 0){
						foundFactor = true;
						break;
					}
					else{
						++primesCnt;
					}
					//Check if the index has gone out of bounds
					if(primesCnt >= primes.Count){
						break;
					}
				}

				//If you didn't find a factor then the current number must be prime
				if(!foundFactor){
					primes.Add(possiblePrime);
				}
				else{
					foundFactor = false;
				}
			}

			//Sort the list before returning it
			primes.Sort();
			return primes;
		}
		public static List<long> GetNumPrimes(long numberOfPrimes){
			List<long> primes = new List<long>();	//Holds the prime numbers
			bool foundFactor = false;	//A flag for whether a factor of the current number has been found

			//If the number is 0 or negative return an empty list
			if(numberOfPrimes <= 1){
				return primes;
			}
			//Otherwise the number is at least 2, so 2 should be added to the list
			else{
				primes.Add(2);
			}

			//We can now start at 3 and skip all even numbers, because they cannot be prime
			for(long possiblePrime = 3;primes.Count < numberOfPrimes;possiblePrime += 2){
				//Check all current primes, up to sqrt(possiblePrime), to see if there is a divisor
				long topPossibleFactor = (long)Math.Ceiling(Math.Sqrt(possiblePrime));
				//We can safely assume that there will be at least 1 element in the primes list because of 2 being added before this
				for(int primesCnt = 0;primes[primesCnt] <= topPossibleFactor;){
					if((possiblePrime % primes[primesCnt]) == 0){
						foundFactor = true;
						break;
					}
					else{
						++primesCnt;
					}
					//Check if the index has gone out of bounds
					if(primesCnt >= primes.Count){
						break;
					}
				}

				//If you didn't find a factor then the current number must be prime
				if(!foundFactor){
					primes.Add(possiblePrime);
				}
				else{
					foundFactor = false;
				}
			}

			//Sort the list before returning it
			primes.Sort();
			return primes;
		}
		public static List<BigInteger> GetNumPrimes(BigInteger numberOfPrimes){
			List<BigInteger> primes = new List<BigInteger>();	//Holds the prime numbers
			bool foundFactor = false;	//A flag for whether a factor of the current number has been found

			//If the number is 0 or negative return an empty list
			if(numberOfPrimes <= 1){
				return primes;
			}
			//Otherwise the number is at least 2, so 2 should be added to the list
			else{
				primes.Add(2);
			}

			//We can now start at 3 and skip all even numbers, because they cannot be prime
			for(BigInteger possiblePrime = 3;primes.Count < numberOfPrimes;possiblePrime += 2){
				//Check all current primes, up to sqrt(possiblePrime), to see if there is a divisor
				BigInteger topPossibleFactor = BigIntegerSqrt(possiblePrime);
				//We can safely assume that there will be at least 1 element in the primes list because of 2 being added before this
				for(int primesCnt = 0;primes[primesCnt] <= topPossibleFactor;){
					if((possiblePrime % primes[primesCnt]) == 0){
						foundFactor = true;
						break;
					}
					else{
						++primesCnt;
					}
					//Check if the index has gone out of bounds
					if(primesCnt >= primes.Count){
						break;
					}
				}

				//If you didn't find a factor then the current number must be prime
				if(!foundFactor){
					primes.Add(possiblePrime);
				}
				else{
					foundFactor = false;
				}
			}

			//Sort the list before returning it
			primes.Sort();
			return primes;
		}
		//This function determines whether the number passed into it is a prime
		public static bool IsPrime(int possiblePrime){
			if(possiblePrime <= 3){
				return possiblePrime > 1;
			}
			else if(((possiblePrime % 2) == 0) || ((possiblePrime % 3) == 0)){
				return false;
			}
			for(int cnt = 5;(cnt * cnt) <= possiblePrime;cnt += 6){
				if(((possiblePrime % cnt) == 0) || ((possiblePrime % (cnt + 2)) == 0)){
					return false;
				}
			}
			return true;
		}
		public static bool IsPrime(long possiblePrime){
			if(possiblePrime <= 3){
				return possiblePrime > 1;
			}
			else if(((possiblePrime % 2) == 0) || ((possiblePrime % 3) == 0)){
				return false;
			}
			for(long cnt = 5;(cnt * cnt) <= possiblePrime;cnt += 6){
				if(((possiblePrime % cnt) == 0) || ((possiblePrime % (cnt + 2)) == 0)){
					return false;
				}
			}
			return true;
		}
		public static bool IsPrime(BigInteger possiblePrime){
			if(possiblePrime <= 3){
				return possiblePrime > 1;
			}
			else if(((possiblePrime % 2) == 0) || ((possiblePrime % 3) == 0)){
				return false;
			}
			for(BigInteger cnt = 5;(cnt * cnt) <= possiblePrime;cnt += 6){
				if(((possiblePrime % cnt) == 0) || ((possiblePrime % (cnt + 2)) == 0)){
					return false;
				}
			}
			return true;
		}
		//These functions return all the divisors of goalNumber
		public static List<int> GetDivisors(int goalNumber){
			List<int> divisors = new List<int>();
			//Start by checking that ht enumber is positive
			if(goalNumber <= 0){
				return divisors;
			}
			//If the number is 1 return just itself
			else if(goalNumber == 1){
				divisors.Add(1);
				return divisors;
			}

			//Start at 3 and loop through all numbers < sqrt(goalNumber) looking for a number that divides it evenly
			double topPossibleDivisor = Math.Ceiling(Math.Sqrt(goalNumber));
			for(int possibleDivisor = 1;possibleDivisor <= topPossibleDivisor;++possibleDivisor){
				//If you find one add it and the number it creates to the list
				if((goalNumber % possibleDivisor) == 0){
					divisors.Add(possibleDivisor);
					//Account for the pssibility of sqrt(goalNumber) being a divisor
					if(possibleDivisor != topPossibleDivisor){
						divisors.Add(goalNumber / possibleDivisor);
					}
					//Take care of a few occations where a number was added twice
					if(divisors[divisors.Count - 1] == (possibleDivisor + 1)){
						++possibleDivisor;
					}
				}
			}

			//Sort the list before returning it for neatness
			divisors.Sort();
			//Return the list
			return divisors;
		}
		public static List<long> GetDivisors(long goalNumber){
			List<long> divisors = new List<long>();
			//Start by checking that ht enumber is positive
			if(goalNumber <= 0){
				return divisors;
			}
			//If the number is 1 return just itself
			else if(goalNumber == 1){
				divisors.Add(1);
				return divisors;
			}

			//Start at 3 and loop through all numbers < sqrt(goalNumber) looking for a number that divides it evenly
			double topPossibleDivisor = Math.Ceiling(Math.Sqrt(goalNumber));
			for(long possibleDivisor = 1;possibleDivisor <= topPossibleDivisor;++possibleDivisor){
				//If you find one add it and the number it creates to the list
				if((goalNumber % possibleDivisor) == 0){
					divisors.Add(possibleDivisor);
					//Account for the pssibility of sqrt(goalNumber) being a divisor
					if(possibleDivisor != topPossibleDivisor){
						divisors.Add(goalNumber / possibleDivisor);
					}
					//Take care of a few occations where a number was added twice
					if(divisors[divisors.Count - 1] == (possibleDivisor + 1)){
						++possibleDivisor;
					}
				}
			}

			//Sort the list before returning it for neatness
			divisors.Sort();
			//Return the list
			return divisors;
		}
		public static List<BigInteger> GetDivisors(BigInteger goalNumber){
			List<BigInteger> divisors = new List<BigInteger>();
			//Start by checking that ht enumber is positive
			if(goalNumber <= 0){
				return divisors;
			}
			//If the number is 1 return just itself
			else if(goalNumber == 1){
				divisors.Add(1);
				return divisors;
			}

			//Start at 3 and loop through all numbers < sqrt(goalNumber) looking for a number that divides it evenly
			double topPossibleDivisor = (double)BigIntegerSqrt(goalNumber);
			for(long possibleDivisor = 1;possibleDivisor <= topPossibleDivisor;++possibleDivisor){
				//If you find one add it and the number it creates to the list
				if((goalNumber % possibleDivisor) == 0){
					divisors.Add(possibleDivisor);
					//Account for the pssibility of sqrt(goalNumber) being a divisor
					if(possibleDivisor != topPossibleDivisor){
						divisors.Add(goalNumber / possibleDivisor);
					}
					//Take care of a few occations where a number was added twice
					if(divisors[divisors.Count - 1] == (possibleDivisor + 1)){
						++possibleDivisor;
					}
				}
			}

			//Sort the list before returning it for neatness
			divisors.Sort();
			//Return the list
			return divisors;
		}
		//This function returns the goalSubscript'th Fibonacci number
		public static int GetFib(int goalSubscript){
			//Setup the variables
			int[] fibNums = {1, 1, 0};	//A list to keep track of the Fibonacci numbers. It need only be 3 long because we only need the one we are working on and the last 2

			//If the number is <= 0 return 0
			if(goalSubscript <= 0){
				return 0;
			}

			//Loop through the list, generating Fibonacci numbers until it finds the correct subscript
			int fibLoc;
			for(fibLoc = 2;fibLoc < goalSubscript;++fibLoc){
				fibNums[fibLoc % 3] = fibNums[(fibLoc - 1) % 3] + fibNums[(fibLoc - 2) % 3];
			}

			//Return the proper number. The location counter is 1 off of the subscript
			return fibNums[(fibLoc - 1) % 3];
		}
		public static long GetFib(long goalSubscript){
			//Setup the variables
			long[] fibNums = {1, 1, 0};	//A list to keep track of the Fibonacci numbers. It need only be 3 long because we only need the one we are working on and the last 2

			//If the number is <= 0 return 0
			if(goalSubscript <= 0){
				return 0;
			}

			//Loop through the list, generating Fibonacci numbers until it finds the correct subscript
			long fibLoc;
			for(fibLoc = 2;fibLoc < goalSubscript;++fibLoc){
				fibNums[fibLoc % 3] = fibNums[(fibLoc - 1) % 3] + fibNums[(fibLoc - 2) % 3];
			}

			//Return the proper number. The location counter is 1 off of the subscript
			return fibNums[(fibLoc - 1) % 3];
		}
		public static BigInteger GetFib(BigInteger goalSubscript){
			//Setup the variables
			BigInteger[] fibNums = {1, 1, 0};	//A list to keep track of the Fibonacci numbers. It need only be 3 long because we only need the one we are working on and the last 2

			//If the number is <= 0 return 0
			if(goalSubscript <= 0){
				return 0;
			}

			//Loop through the list, generating Fibonacci numbers until it finds the correct subscript
			long fibLoc;
			for(fibLoc = 2;fibLoc < goalSubscript;++fibLoc){
				fibNums[fibLoc % 3] = fibNums[(fibLoc - 1) % 3] + fibNums[(fibLoc - 2) % 3];
			}

			//Return the proper number. The location counter is 1 off of the subscript
			return fibNums[(fibLoc - 1) % 3];
		}
		//This function returns the GCD of the two numbers sent to it
		public static int GCD(int num1, int num2){
			while((num1 != 0) && (num2 != 0)){
				if(num1 > num2){
					num1 %= num2;
				}
				else{
					num2 %= num1;
				}
			}
			return num1 | num2;
		}
		public static long GCD(long num1, long num2){
			while((num1 != 0) && (num2 != 0)){
				if(num1 > num2){
					num1 %= num2;
				}
				else{
					num2 %= num1;
				}
			}
			return num1 | num2;
		}
		public static BigInteger GCD(BigInteger num1, BigInteger num2){
			while((num1 != 0) && (num2 != 0)){
				if(num1 > num2){
					num1 %= num2;
				}
				else{
					num2 %= num1;
				}
			}
			return num1 | num2;
		}
		//Return the factorial of the number passed in
		public static int Factorial(int num){
			int fact = 1;
			for(int cnt = 1;cnt <= num;++cnt){
				fact *= cnt;
			}
			return fact;
		}
		public static long Factorial(long num){
			long fact = 1;
			for(long cnt = 1;cnt <= num;++cnt){
				fact *= cnt;
			}
			return fact;
		}
		public static BigInteger Factorial(BigInteger num){
			BigInteger fact = 1;
			for(BigInteger cnt = 1;cnt <= num;++cnt){
				fact *= cnt;
			}
			return fact;
		}
		//Converts a number to its binary equivalent
		public static string ToBin(int num){
			//Convert the number to a binary string
			string binaryString = Convert.ToString(num, 2);
			return binaryString;
		}
		public static string ToBin(long num){
			//Convert the number to a binary string
			string binaryString = Convert.ToString(num, 2);
			return binaryString;
		}
		public static string ToBin(BigInteger num){
			//Convert the number to a binary string
			byte[] bytes = num.ToByteArray();
			bool[] bits = new bool[8 * bytes.Length];
			new System.Collections.BitArray(bytes).CopyTo(bits, 0);
			bits = bits.Reverse().ToArray();
			System.Text.StringBuilder tempString = new System.Text.StringBuilder();
			foreach(bool bit in bits){
				tempString.Append(bit ? '1' : '0');
			}
			//Remove any leading 0's
			string binaryString = System.Text.RegularExpressions.Regex.Replace(tempString.ToString(), @"^0+", "");
			//If nothing is left make sure there is a 0
			if(binaryString == ""){
				binaryString = "0";
			}
			return binaryString;
		}
		//Sieve of Eratosthenese for generating primes
		public static IEnumerable<long> SieveOfEratosthenes(){
			yield return 2;

			//A dictionary for holding the results of all primes that have already been found
			Dictionary<long, List<long>> dict = new Dictionary<long, List<long>>();

			//Start checking for primes with the number 3 and skip all even numbers
			for(long possiblePrime = 3;true;possiblePrime += 2){
				//If possiblePrime is not in the dictionary it is a new prime number
				//Return it and mark its next multiple
				if(!dict.ContainsKey(possiblePrime)){
					yield return possiblePrime;
					List<long> tempList = new List<long>(){possiblePrime};
					dict.Add(possiblePrime * possiblePrime, tempList);
				}
				//If possiblePrime is in the dictionary it is a composite number
				else{
					//Move each witness to it's next multiple
					foreach(long num in dict[possiblePrime]){
						if(dict.ContainsKey(num + num + possiblePrime)){
							dict[num + num + possiblePrime].Add(num);
						}
						else{
							List<long> tempList = new List<long>(){num};
							dict.Add(num + num + possiblePrime, tempList);
						}
					}
					//We no longer need this, free the memory
					dict.Remove(possiblePrime);
				}
			}
		}
		public static IEnumerable<BigInteger> SieveOfEratosthenesBig(){
			yield return 2;

			//A dictionary for holding the results of all primes that have already been found
			Dictionary<BigInteger, List<BigInteger>> dict = new Dictionary<BigInteger, List<BigInteger>>();

			//Start checking for priems with the number 3 and skip all even numbers
			for(BigInteger possiblePrime = 3;true;possiblePrime += 2){
				//If possiblePrime is not in the dictionary it is a new priem number
				//Return it and mark its next multiple
				if(!dict.ContainsKey(possiblePrime)){
					yield return possiblePrime;
					List<BigInteger> tempList = new List<BigInteger>(){possiblePrime};
					dict.Add(possiblePrime * possiblePrime, tempList);
				}
				//If possiblePrime is in the dictionary it is a composite number
				else{
					//Move each witness to ti's next multiple
					foreach(BigInteger num in dict[possiblePrime]){
						if(dict.ContainsKey(num + num + possiblePrime)){
							dict[num + num + possiblePrime].Add(num);
						}
						else{
							List<BigInteger> tempList = new List<BigInteger>(){num};
							dict.Add(num + num + possiblePrime, tempList);
						}
					}
					//We no longer need this, free the memory
					dict.Remove(possiblePrime);
				}
			}
		}
	}
}
